function displayTime() {
    document.querySelector('#digit-clock').innerHTML = "Current time: " + new Date();
}

setInterval(displayTime, 500);

var canvas = document.querySelector('#analog-clock');
var ctx = canvas.getContext('2d');
var radius = canvas.height / 2;
ctx.translate(radius, radius);
radius = radius * 0.9;
setInterval(drawClock, 100);

function drawClock() {
    drawFace(ctx, radius);
    drawNumbers(ctx, radius);
    drawTime(ctx, radius);
}

function getZodiacSign() {
    let dateOfBirth = $('#birthday').val();

    $('#sign').html('');

    let reqUrl = `https://cca-zhengy6-ganarajuv1.azurewebsites.net/zodiac?date=${dateOfBirth}`;
    $.get(reqUrl, (result) => {
        $('#sign').html(`Your zodiac sign is ${result}`);
    });
    $('#birthday').val('')
}

function getFamousBirthdates() {
    let startyear = $('#startyear').val();
    let endyear = $('#endyear').val();

    $('#celebrities').html('');

    let reqUrl = `https://cca-team8-microservice2.herokuapp.com/famousBirthdates?startyear=${startyear}&endyear=${endyear}`;
    $.get(reqUrl, (result) => {
            let recvData = '';
            result.forEach(celebrity => {
                recvData = recvData + `${celebrity.firstname} ${celebrity.lastname}, Birthday: ${celebrity.date}-${celebrity.year}, Zodiac: ${celebrity.zodiac}<br>`;
            });
            $('#celebrities').html(recvData);
        }
    );
    $('#startyear').val('');
    $('#endyear').val('');
}